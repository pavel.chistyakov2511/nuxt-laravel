<?php

namespace  App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\Coupon;

class CouponRedeemed {
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $coupon;

    public function __construct(Coupon $coupon) {
        $this->coupon = $coupon;
    }

}
