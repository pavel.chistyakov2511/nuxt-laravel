<?php

namespace App\Http\Controllers\Api\v1;

use App\Imports\CategoriesImport;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\v1\Category;
use App\Http\Controllers\Api\BaseController as BaseController;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductsImport;

class CategoriesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = ['name'=>$request->get('category')['name'],
                 'slug'=>Str::slug($request->get('category')['name'])];

        if(!isset($request->get('category')['category_id'])){
            $node = new Category($data);
            $node->saveAsRoot();
        }else{
            $node = new Category($data);
            $parent = Category::find($request->get('category')['category_id']);
            $node->appendToNode($parent)->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list(Request $request){
        if(isset($request['deep'])){
            $categories = Category::where('depth', '>=', $request['deep'])->where('status', 0)->hasChildren()->get();
        }else{
            $categories = Category::where('depth', '>=', 1)->where('status', 0)->get();
        }
        return response()->json($categories);
    }

    public static function upload(){
        $import = new CategoriesImport();
        $import->CategoryRepository = new Category;
        $import->id = 1;
        $import->parent_id = 0;

        $path = storage_path('app').'/framework/categoriesUpload/categories.xlsx';

        Excel::import($import, $path);

    }
}
