<?php

namespace App\Http\Controllers\API\v1;

use App\Imports\ProductsImport;
use App\Models\Distributor;
use App\Models\v1\Organization;
use App\Models\v1\Product;
use App\Models\v1\Tender;
use Illuminate\Http\Request;
use App\Models\v1\Category;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use App\Models\v1\Region;
use App\Services\ProductService;
use Vanilo\Properties\Models\Property;
use Vanilo\Properties\Models\PropertyValue;
use App\Models\ProductsEnum;
use Vanilo\Product\Models\ProductStateProxy;
use Illuminate\Support\Facades\Auth;
use App\Models\ModelPropertyValue;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Services\CategoryService;

class ProductsController extends Controller
{

    private $lastrow;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type_organization = Organization::select('company_type')->where('user_id', Auth::id())->first();

        if($type_organization->company_type === 'manufacturer'){
            $products = Product::where('user_id', Auth::id())->get();
            foreach ($products as $id=>$product){
                $products[$id]['properties'] = Product::properties($product->id);
                $product->getImageUrls();
            }
            $data['products'] = $products;
            $data['is_manufacturer'] = true;
            return response($data)
                ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
                ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
                ->header("Access-Control-Allow-Credentials", 'true');
        }

        $tenders = Tender::getTenders();

        $companies_id = $c = [];

        $found = false;

        foreach ($tenders as $tender){

            $companies = Organization::where('min_sum_order','<=',$tender->sum)
                ->where('company_type','manufacturer')
                ->get();

            if(empty($companies))
                continue;

            $parameters = json_decode($tender->parameters);

            $sizes = $parameters->sizes;

            $product_id = ProductService::getTenderProductProperty($sizes);

            if(empty($product_id))
                continue;
            else
                $products_id[] = $product_id;


            $segments = json_decode($tender->segment);

            foreach($segments as $segment){
                $s[] = $segment->name;
            }

            $categories = json_decode($tender->categories);

            /*foreach ($categories as $category){
                $categories_data = Category::defaultOrder()->descendantsAndSelf($category->category_id);
                foreach ($categories_data as $cat){
                    $c[] = $cat->id;
                }

            }*/

            $categories_data = Category::defaultOrder()->descendantsAndSelf($categories);
            foreach ($categories_data as $cat){
                $c[] = $cat->id;
            }


            foreach ($companies as $company){
               $companies_id[] = $company->seller_id;
            }

            $found = true;

        }

        if($found){
            foreach ($products_id as $ids){
                foreach ($ids as $id){
                    $products_ids[] = $id;
                }
            }
            $products = Product::whereIn('id', $products_ids)
                ->whereIn('company_id', $companies_id)
                ->where(function($query) use($s){
                   foreach($s as $segment) {
                       $query->orWhereJsonContains('segment', [['name' => $segment]]);
                   }
                })
                ->whereIn('category_id', $c)
                ->where('stock', '>', 0)
                ->get();

            foreach ($products as $id=>$product){

                $products[$id]['properties'] = Product::properties($product->id);
                $product->getThumbnailUrls();

                $product_weight = ProductService::getProductSort($product);

                $products_size = sizeof($products);

                if($product_weight > 0){
                    $filtered_sort_id = $products_size + $product_weight - $id;
                }else{
                    $filtered_sort_id = $id;
                }


                $filtered_sort[$filtered_sort_id] = $products[$id];
            }
        }else{
            $filtered_sort = [];
        }

        rsort($filtered_sort);

        $data['products'] = $filtered_sort;
        $data['is_manufacturer'] = false;

        return response($data)
            ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
            ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
            ->header("Access-Control-Allow-Credentials", 'true');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('depth', '>=', 1)->get();
        $regions = Region::where('depth', 1)->get();
        $colors = Property::findBySlug('color');
        $colors = PropertyValue::byProperty($colors)->get();
        $sizes = Property::findBySlug('size');
        $sizes = PropertyValue::byProperty($sizes)->get();

        $products = Product::where('user_id', Auth::id())->get();

        $statuses = [
            ProductsEnum::NEW_STATE         => 'Новинка',
            ProductsEnum::DRAFT_STATE       => 'Черновик',
            ProductsEnum::ACTIVE_STATE      => 'Активно',
            ProductsEnum::INACTIVE_STATE    => 'Не активно',
            ProductsEnum::UNAVAILABLE_STATE => 'Недоступно',
            ProductsEnum::PROMO_STATE       => 'Акция',
            ProductsEnum::HIDDEN_STATE      => 'Скрыто',
            ProductsEnum::DELETED_STATE     => 'Удалено',
            ProductsEnum::RECOMMENDED_STATE => 'Рекомендуемый',
            ProductsEnum::REPEAT_STATE      => 'Ожидается в повторе',
            ProductsEnum::PREORDER_STATE    => 'Предзаказ'
        ];

        return response()->json( [
            'categories'=>$categories,
            'regions'=>$regions,
            'sizes'=>$sizes,
            'colors'=>$colors,
            'statuses'=>$statuses,
            'products'=>$products,
        ] );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request['product_modification'] != 0){
            $product_id = $request['product_modification'];
            $is_modification = true;
        }
        else{
            $product_id = Str::uuid();
            $is_modification = false;
        }

        if(!empty($request["product_segment"])){
            $segments_data = json_decode($request["product_segment"]);
            foreach ($segments_data as $segment){
                $segments[]['name'] = $segment->value;
            }
        }

        $company_id = Organization::select('id')->where('user_id', Auth::id())->first();

        $product_data = [
            "product_id" => $product_id,
            "company_id" => $company_id->id,
            "name" => trim($request["product_name"]) ?? '',
            //"barcode" => $request["product_barcode"] ?? 0000000,
            "sku" => trim($request["product_sku"]) ?? '',
            "user_id" => Auth::id(),
            "model" => $request["product_model"] ?? '',
            "country_made_in" => $request["product_region"] ?? 0,
            "description" => $request["product_description"] ?? '',
            "category_id" => $request["product_category"] ?? 0,
            "category_group_id" => $request["product_base_category"] ?? 0,
            "price" => $request["product_base_price"] ?? 0,
            //"dropshipping_price" => $request["product_drop_price"],
            "discount" => $request["product_discount"] ?? 0,
            "sale" =>  $request["product_sales"],
            "recommended_retail_price" => $request["product_price"] ?? 0,
            "min_order" => $request["product_min_order"] ?? 1,
            "status" => $request["product_status"] ?? 'hidden',
            "segment" => json_encode($segments) ?? '',
            "collection" => $request["product_collection"] ?? '',
            //"stock" => array_sum($request['product_sizes'] ?? []),
            'is_modification' => $is_modification,
            "consist" => $request["product_consist"] ?? '',
            "modification" => $request['product_modification_id'] ?? 0,
        ];


        $product = ProductService::saveProduct($product_data);


        if(!empty($request["product_sizes"])) {
            $product_sizes = json_decode($request["product_sizes"]);
            $barcodes = json_decode($request["product_barcodes"]);
            foreach ($product_sizes as $key => $size) {
                $property_size_id = DB::table('property_values')
                    ->select('property_values.id')
                    ->where('property_values.value', $key)
                    ->first();

                    DB::insert('insert into model_property_values (property_value_id, model_type, model_id, stock, created_at, barcode) values (?, ?, ?, ?, ?, ?)', [$property_size_id->id, 'product', $product->id, $size, now(), $barcodes->{$key}]);
            }
        }

        DB::insert('insert into model_property_values (property_value_id, model_type,model_id,stock) values (?, ?, ?, ?)', [$request["product_color"],'product',$product->id, 0]);

        $model_propery_values = DB::table('model_property_values')->select('stock')->where('model_id', $product->id)->get();
        $stock = collect($model_propery_values)->sum('stock');

        ProductService::updateProduct(['stock'=>$stock], $product->id);

        $images = $request->file('product_images');

        if(!empty($images)){
            foreach ($images as $image){

                $directory = 'images/products/'.$product->id;

                $file_name = $directory."/".Str::random(25).substr($image, strripos($image, '.'));

                $file = file_get_contents($image);
                Storage::disk('local')->put($file_name, $file);

                $product->addMedia(storage_path('app').'/'.$file_name)->toMediaCollection();
            }
        }

        return response(['slug'=>$product->slug])
            ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
            ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
            ->header("Access-Control-Allow-Credentials", 'true');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $organization = Organization::where('user_id', Auth::id())->first();

        $product = Product::where('slug', $request->get('slug'))->first();

        $properties = Product::properties($product->id);

        $product->getThumbnailUrls();

        $brand = Organization::select('id','brand', 'seller_id')->where('user_id',$product->user_id)->first();

        $distributor = Distributor::where('customer_id', $organization->customer_id)->where('seller_id', $brand->seller_id)->get();

        if(strpos($product->discount,'%') === false){
            $discount = $product->discount;
            $percent = false;
        }else{
            $discount = substr($product->discount, 0, strpos($product->discount, '%'));
            $discount = $discount / 100;
            $percent = true;
        }

        $data = ['product'=>$product, 'brand'=>$brand, 'distributor'=>$distributor, 'properties'=>$properties];

        if($organization->company_type === 'manufacturer'){
            $data['is_manufacturer'] = true;
        }else{
            $data['is_manufacturer'] = false;
        }

        $data['is_percent'] = $percent;
        $data['discount'] = $discount;

        return response($data)
            ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
            ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
            ->header("Access-Control-Allow-Credentials", 'true');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $baseCategories = CategoryService::getBaseCategores();

        $categories = CategoryService::getLastCategories();

        $regions = Region::where('depth', 1)->get();

        $colors = Property::findBySlug('color');
        $colors = PropertyValue::byProperty($colors)->get();

        $sizes = Property::findBySlug('size');
        $sizes = PropertyValue::byProperty($sizes)->get();

        $texture = Property::findBySlug('texture');
        $texture = PropertyValue::byProperty($texture)->get();

        $user_id = Auth::id();

        $products = Cache::rememberForever('user_id'.$user_id, function () {
            return Product::where('user_id', Auth::id())->get();
        });

        $product = Product::where('slug', $request->get('slug'))->first();

        $product->getThumbnailUrls();

        $statuses = [
            ProductsEnum::NEW_STATE         => 'Новинка',
            ProductsEnum::DRAFT_STATE       => 'Черновик',
            ProductsEnum::ACTIVE_STATE      => 'Активно',
            ProductsEnum::INACTIVE_STATE    => 'Не активно',
            ProductsEnum::UNAVAILABLE_STATE => 'Недоступно',
            ProductsEnum::PROMO_STATE       => 'Акция',
            ProductsEnum::HIDDEN_STATE      => 'Скрыто',
            ProductsEnum::DELETED_STATE     => 'Удалено',
            ProductsEnum::RECOMMENDED_STATE => 'Рекомендуемый',
            ProductsEnum::REPEAT_STATE      => 'Ожидается в повторе',
            ProductsEnum::PREORDER_STATE    => 'Предзаказ'
        ];

        $params = $product->properties($product->id);

        if(isset($params['color'])){
            $color = $params['color'];
        }else{
            $color = null;
        }

        $category_type = null;

        $selected = $product->category_group_id;

        foreach ($baseCategories as $bCat){
            if($bCat->id == $product->category_group_id){
                $selected = $bCat;
            }
        }

        if($product->category_id > 0){
            foreach ($categories[$product->category_group_id] as $category){
                if($category->id == $product->category_id){
                    $category_type = $category->type;
                    $productSelectedCategory = $category;
                    break;
                }
            }
        }else{
            $productSelectedCategory = $selected;
        }


        $modification = null;

        foreach ($products as $productData){
            if($productData->id == $product->modification){
                $modification = $productData;
                break;
            }
        }

        $sizes_data = [];

        if(isset($params['size'])){
            foreach ($sizes as $size){
                if($size->category_type == $category_type){
                    $isProductSize = false;
                    foreach ($params['size'] as $product_size){
                        if($product_size->id == $size->id){
                            $sizes_data[] = $product_size;
                            $isProductSize = true;
                            break;
                        }
                    }
                    if(!$isProductSize){
                        $sizes_data[] = $size;
                    }
                }
            }
        }else{
            $sizes_data = null;
        }

        $segments = [
            'low-priced'=>'Эконом',
            'middle-priced'=>'Средний',
            'high-priced'=>'Высокий',
            'luxury'=>'Премиум',
        ];

        $product_segments = json_decode($product['segment']);

        $product_segments_data = [];
        $product_status = [];

            foreach ($statuses as $value=>$title){
                if($product->status == $value){
                    $product_status[] = [
                        'value' => $value,
                        'title' => $title
                    ];
                    break;
                }
            }

        foreach($product_segments as $product_segment){
            foreach ($segments as $value=>$title){
                if($product_segment->name == $value){
                    $product_segments_data[] = [
                        'value' => $value,
                        'title' => $title
                    ];
                }
            }
        }

        $data = [
            'baseCategories' => $baseCategories,
            'selected' => $selected,
            'productSelectedCategory' => $productSelectedCategory,
            'product' => $product,
            'color'=>$color,
            'sizes_data'=>$sizes_data,
            'segments' => $product_segments_data,
            'categories'=>$categories,
            'regions'=>$regions,
            'sizes'=>$sizes,
            'colors'=>$colors,
            'texture'=>$texture,
            'statuses'=>$product_status,
            'products'=>$products,
            'modification'=>$modification
        ];


        return response($data)
            ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
            ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
            ->header("Access-Control-Allow-Credentials", 'true');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id = null)
    {

        $product = Product::find($request->get('product_id'));

        if($product->user_id == Auth::id()) {

            if ($request['product_modification'] != 0) {
                $product_id = $request['product_modification'];
                $is_modification = true;
            } else {
                $is_modification = false;
            }


            if(!empty($request["product_segment"])){
                $segments_data = json_decode($request["product_segment"]);
                foreach ($segments_data as $segment){
                    $segments[]['name'] = $segment->value;
                }
            }

            $product_data = [
                "product_id" => $product_id ?? $product->product_id,
                "name" => $request["product_name"] ?? '',
                "sku" => $request["product_sku"] ?? '',
                "model" => $request["product_model"] ?? '',
                "country_made_in" => $request["product_region"] ?? 0,
                "description" => $request["product_description"] ?? '',
                "consist" => $request["product_consist"] ?? '',
                "category_id" => $request["product_category"] ?? 0,
                "category_group_id" => $request["product_base_category"] ?? 0,
                "price" => $request["product_base_price"] ?? 0,
                //"dropshipping_price" => $request["product_drop_price"],
                "discount" => $request["product_discount"] ?? 0,
                "recommended_retail_price" => $request["product_price"] ?? 0,
                "min_order" => $request["product_min_order"] ?? 1,
                "status" => $request["product_status"] ?? 'hidden',
                "segment" => $segments ?? '',
                "collection" => $request["product_collection"] ?? '',
                //"stock" => array_sum($request['product_sizes'] ?? 0),
                'is_modification' => $is_modification,
                "modification" => $request['product_modification_id'] ?? 0,
            ];

            ProductService::updateProduct($product_data, $product->id);

            if(!empty($request["product_sizes"])) {
                $product_sizes = json_decode($request["product_sizes"]);
                foreach ($product_sizes as $key => $size) {
                    $property_size_id = DB::table('property_values')
                        ->select('property_values.id')
                        ->where('property_values.value', $key)
                        ->first();

                    $model_propery_values = DB::table('model_property_values')->where('property_value_id', $property_size_id->id)->where('model_id', $product->id)->first();
                    if (!empty($model_propery_values)) {
                        DB::update(
                            'update model_property_values set stock = ?, updated_at = ? where property_value_id = ? and model_id = ?',
                            [$size, now(), $property_size_id->id, $product->id]
                        );
                    } else {
                        DB::insert('insert into model_property_values (property_value_id, model_type, model_id, stock, created_at) values (?, ?, ?, ?, ?)', [$property_size_id->id, 'product', $product->id, $size, now()]);
                    }
                }
            }

            if(!empty($request["product_barcodes"])) {
                $barcodes = json_decode($request["product_barcodes"]);
                foreach ($barcodes as $key => $barcode) {
                    $property_size_id = DB::table('property_values')
                                        ->select('property_values.id')
                                        ->where('property_values.value', $key)
                                        ->first();

                    DB::update(
                      'update model_property_values set updated_at = ?, barcode = ? where property_value_id = ? and model_id = ?',
                        [now(), $barcode, $property_size_id->id, $product->id]
                    );
                }
            }

            $model_propery_values = DB::table('model_property_values')->select('stock')->where('model_id', $product->id)->get();
            $stock = collect($model_propery_values)->sum('stock');

            ProductService::updateProduct(['stock'=>$stock], $product->id);

            $images = $request->file('product_images');

            if(!empty($images)){
                foreach ($images as $image){

                    $directory = 'images/products/'.$product->id;

                    $file_name = $directory."/".Str::random(25).substr($image, strripos($image, '.'));

                    $file = file_get_contents($image);
                    Storage::disk('local')->put($file_name, $file);

                    $product->addMedia(storage_path('app').'/'.$file_name)->toMediaCollection();
                }
            }



            /*$data = [];

            return response($data)
                ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
                ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
                ->header("Access-Control-Allow-Credentials", 'true');*/
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function uploadFile(Request $request){

        if(!isset($request->total)){
            $totalRows = $this->getTotalRows($request->file('file')) - 1;
            $file = md5('User'.$request->user()->id).'.'.$request->file('file')->extension();
            $path = $request->file('file')->storeAs(
                'framework/productsUpload', $file
            );
        }
        else{
            $totalRows = $request->total;
            $file = $request->file;
            $path = storage_path('app').'/framework/productsUpload/'.$file;
        }

        $import = new ProductsImport();

        $import->setStartRow((int)$request->startRow);
        $import->setLimit();
        $import->lastSKU = null;

        Excel::import($import, $path);

        $response = ['startRow' => $import->startRow() + $import->limit(), 'total'=>$totalRows, 'file'=>$file];

        return response($response)
            ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
            ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
            ->header("Access-Control-Allow-Credentials", 'true');

    }

    private function getTotalRows($file){

        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $temporaryFileFactory = new \Maatwebsite\Excel\Files\TemporaryFileFactory(
            config('excel.temporary_files.local_path',
                config('excel.exports.temp_path',
                    storage_path('framework/laravel-excel'))
            ),
            config('excel.temporary_files.remote_disk')
        );


        $temporaryFile = $temporaryFileFactory->make($fileExtension);
        $currentFile = $temporaryFile->copyFrom($file,null);
        $reader = \Maatwebsite\Excel\Factories\ReaderFactory::make(null,$currentFile);
        $info = $reader->listWorksheetInfo($currentFile->getLocalPath());
        $totalRows = 0;
        foreach ($info as $sheet) {
            $totalRows += $sheet['totalRows'];
        }
        $currentFile->delete();

        return $totalRows;
    }

    public function sizesList(){
        $sizes = Property::findBySlug('size');
        $sizes = PropertyValue::byProperty($sizes)->get();
        return response($sizes)
            ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
            ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
            ->header("Access-Control-Allow-Credentials", 'true');
    }

    public function removeImg(Request $request){
        var_dump($request->file);
    }
}
