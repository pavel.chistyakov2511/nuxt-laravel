<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\v1\Product;
//use Vanilo\Cart\Facades\Cart;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Vanilo\Cart\Models\CartItem;
use Vanilo\Cart\Models\CartProxy;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = new Cart();
        $items = $cart->getItems();

        return response($items)
            ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
            ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
            ->header("Access-Control-Allow-Credentials", 'true');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (config('vanilo.cart.auto_assign_user') && Auth::check()) {
            $attributes = [
                'user_id' => Auth::user()->id
            ];
        }

        $cart_id = Cart::where('user_id', Auth::user()->id)->first();
        if(empty($cart_id->id)){
            $cart_id = CartProxy::create($attributes ?? []);
        }

        $product = Product::find($request->product_id);
        $stocks = json_encode($request->sizes);
        $cartItem = $cart_id->addItem($product, 1, ['attributes'=>['cart_id'=>$cart_id->id]]);

        $cartItemStocks = json_decode($cartItem->stocks);

        $properties = $product->properties($product->id);

        $stock_summ = 0;

        if(empty($cartItemStocks)){
            CartItem::where('id',$cartItem->id)->update(['stocks'=>$stocks]);

            foreach ($request->sizes as $key=>$cart_stock){

                if($cart_stock == 0)
                    continue;

                $stock_summ += $cart_stock;
            }
        }else{
            $stocks = json_decode($cartItem->stocks);
            $new_stocks = [];

            foreach ($properties['size'] as $stock){
                $product_sizes[$stock->value] = $stock->stock;
            }

            foreach ($stocks as $key=>$cart_stock){
                $new_stocks[$key] = $cart_stock;
            }

            foreach ($request->sizes as $size_value=>$stock){
                if($stock == 0)
                    continue;
                $founded = false;
                foreach ($stocks as $key=>$cart_stock){
                    if($size_value == $key){
                        if($product_sizes[$size_value] < ($cart_stock + $stock)){
                            $new_stocks[$key] = $product_sizes[$size_value] + $stock;
                        }else{
                            $new_stocks[$key] = $cart_stock + $stock;
                        }
                        $founded = true;
                    }
                }
                if(!$founded){
                    $new_stocks[$size_value] = $stock;
                }
            }
            $stocks = json_encode($new_stocks);
            CartItem::where('id',$cartItem->id)->update(['stocks'=>$stocks]);
        }

        foreach ($properties['size'] as $size){
            foreach ($request->sizes as $size_value=>$stock){

                if($stock == 0)
                    continue;

                if($size->value == $size_value){
                    if(($size->stock - $stock) < 0){
                        $new_stocks = 0;
                    }else{
                        $new_stocks = ($size->stock - $stock);
                    }
                    DB::table('model_property_values')->where('model_id', $product->id)->where('property_value_id', $size->id)->update(['stock'=>$new_stocks]);
                }
            }
        }

        $properties = $product->properties($product->id);

        foreach ($properties['size'] as $size){
            $stock_summ += $size->stock;
        }

        Product::where('id', $product->id)->update(['stock'=>$stock_summ]);


        return response(['sizes' => $properties['size'], 'stock'=>$stock_summ])
            ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
            ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'))
            ->header("Access-Control-Allow-Credentials", 'true');


        //return redirect()->route('lk.cart.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
