<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

class CheckoutController extends Controller {

  public function __invoke(Request $request, Order $order) {

    /** @var \App\Contracts\Gateway $gateway */
    $gateway = new $request->gateway();

    return $gateway->purchase($order, $request);
  }

}
