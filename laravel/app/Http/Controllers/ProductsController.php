<?php

namespace App\Http\Controllers;

use App\Models\Distributor;
use App\Models\Organization;
use App\Models\Product;
use App\Models\Tender;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\View;
use App\Models\Region;
use App\Services\ProductService;
use Vanilo\Properties\Models\Property;
use Vanilo\Properties\Models\PropertyValue;
use App\Models\ProductsEnum;
use Vanilo\Product\Models\ProductStateProxy;
use Illuminate\Support\Facades\Auth;
use App\Models\ModelPropertyValue;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type_organization = Organization::select('company_type')->where('user_id', Auth::id())->first();

        if($type_organization->company_type === 'manufacturer'){
            $products = Product::where('user_id', Auth::id())->paginate(16);
            return view('lk.products.index', ['products'=>$products]);
        }

        $tenders = Tender::getTenders();

        $companies_id = $c = [];

        $found = false;

        foreach ($tenders as $tender){

            $companies = Organization::where('min_sum_order','<=',$tender->sum)
                ->where('company_type','manufacturer')
                ->get();


            if(empty($companies))
                continue;
            else
                $found = true;

            $parameters = json_decode($tender->parameters);

            $sizes = $parameters->sizes;

            $products_id = ProductService::getTenderProductProperty($sizes);

            $segments = json_decode($tender->segment);

            $categories = json_decode($tender->categories);

            foreach ($categories as $category){
                $categories_data = Category::defaultOrder()->descendantsAndSelf($category->category_id);
                foreach ($categories_data as $cat){
                    $c[] = $cat->id;
                }

            }

            foreach ($companies as $company){
               $companies_id[] = $company->id;
            }

        }

        if($found){
            $products = Product::whereIn('id', $products_id)
                ->whereIn('company_id', $companies_id)
                ->whereIn('segment', $segments)
                ->whereIn('category_id', $c)
                ->paginate(16);
        }else{
            $products = [];
        }

        return view('lk.products.index', ['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('depth', '>=', 1)->get();
        $regions = Region::where('depth', 1)->get();
        $colors = Property::findBySlug('color');
        $colors = PropertyValue::byProperty($colors)->get();
        $sizes = Property::findBySlug('size');
        $sizes = PropertyValue::byProperty($sizes)->get();

        $products = Product::where('user_id', Auth::id())->get();

        $statuses = [
            ProductsEnum::NEW_STATE         => 'Новинка',
            ProductsEnum::DRAFT_STATE       => 'Черновик',
            ProductsEnum::ACTIVE_STATE      => 'Активно',
            ProductsEnum::INACTIVE_STATE    => 'Не активно',
            ProductsEnum::UNAVAILABLE_STATE => 'Недоступно',
            ProductsEnum::PROMO_STATE       => 'Акция',
            ProductsEnum::HIDDEN_STATE      => 'Скрыто',
            ProductsEnum::DELETED_STATE     => 'Удалено',
            ProductsEnum::RECOMMENDED_STATE => 'Рекомендуемый',
            ProductsEnum::REPEAT_STATE      => 'Ожидается в повторе',
            ProductsEnum::PREORDER_STATE    => 'Предзаказ'
        ];

        return response()->json( [
            'categories'=>$categories,
            'regions'=>$regions,
            'sizes'=>$sizes,
            'colors'=>$colors,
            'statuses'=>$statuses,
            'products'=>$products,
        ] );


        /*return view('lk.products.create', [
            'categories'=>$categories,
            'regions'=>$regions,
            'sizes'=>$sizes,
            'colors'=>$colors,
            'statuses'=>$statuses,
            'products'=>$products,
        ]);*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request['product-modification'] != 0){
            $product_id = $request['product-modification'];
            $is_modification = true;
        }
        else{
            $product_id = Str::uuid();
            $is_modification = false;
        }

        $company_id = Organization::select('id')->where('user_id', Auth::id())->first();

        $product_data = [
            "product_id" => $product_id,
            "company_id" => $company_id->id,
            "name" => $request["product-name"] ?? '',
            "barcode" => $request["product-barcode"] ?? 0000000,
            "sku" => $request["product-sku"] ?? '',
            "user_id" => Auth::id(),
            "model" => $request["product-model"],
            "country_made_in" => $request["product-region"] ?? 0,
            "description" => $request["product-description"],
            "category_id" => $request["product-category"] ?? 0,
            "category_group_id" => $request["product-base_category"] ?? 0,
            "price" => $request["product-base_price"] ?? 0,
            "dropshipping_price" => $request["product-drop_price"],
            "discount" => $request["product-discount"] ?? 0,
            "recommended_retail_price" => $request["product-price"] ?? 0,
            "min_order" => $request["product-min_order"] ?? 1,
            "status" => $request["product-status"] ?? 'hidden',
            "segment" => $request["product-segment"] ?? '',
            "stock" => array_sum($request['product-sizes'] ?? []),
            'is_modification' => $is_modification,
        ];

        $product = ProductService::saveProduct($product_data);

        foreach ($request['product-sizes'] as $size_id=>$count){
            if(!is_null($count)){
                DB::insert('insert into model_property_values (property_value_id, model_type,model_id,stock) values (?, ?, ?, ?)', [$size_id,'product',$product->id, $count]);
            }
        }
        DB::insert('insert into model_property_values (property_value_id, model_type,model_id,stock) values (?, ?, ?, ?)', [$request["product-color"],'product',$product->id, 0]);

        flash()->success(__(':name has been created', ['name' => $product->name]));

        try {
            if (!empty($request->files->filter('images'))) {
                $product->addMultipleMediaFromRequest(['images'])->each(function ($fileAdder) {
                    $fileAdder->usingName(Str::random(16))->toMediaCollection();
                });
            }
        } catch (\Exception $e) { // Here we already have the product created
            flash()->error(__('Error: :msg', ['msg' => $e->getMessage()]));

            return redirect()->route('lk.products.create', ['product' => $product]);
        }

        return redirect()->route('lk.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Organization::select('id','brand')->where('user_id',$id->user_id)->get();

        $distributor = Distributor::where('retailer_id', Auth::id())->where('distributor_id', $id->user_id)->get();

        return view('lk.products.show', ['product'=>$id, 'brand'=>$brand, 'distributor'=>$distributor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $id)
    {
        $categories = Category::where('depth', '>=', 1)->get();
        $regions = Region::where('depth', 1)->get();
        $colors = Property::findBySlug('color');
        $colors = PropertyValue::byProperty($colors)->get();
        $sizes = Property::findBySlug('size');
        $sizes = PropertyValue::byProperty($sizes)->get();

        $products = Product::where('user_id', Auth::id())->get();

        $statuses = [
            ProductsEnum::NEW_STATE         => 'Новинка',
            ProductsEnum::DRAFT_STATE       => 'Черновик',
            ProductsEnum::ACTIVE_STATE      => 'Активно',
            ProductsEnum::INACTIVE_STATE    => 'Не активно',
            ProductsEnum::UNAVAILABLE_STATE => 'Недоступно',
            ProductsEnum::PROMO_STATE       => 'Акция',
            ProductsEnum::HIDDEN_STATE      => 'Скрыто',
            ProductsEnum::DELETED_STATE     => 'Удалено',
            ProductsEnum::RECOMMENDED_STATE => 'Рекомендуемый',
            ProductsEnum::REPEAT_STATE      => 'Ожидается в повторе',
            ProductsEnum::PREORDER_STATE    => 'Предзаказ'
        ];

        $params = $id->properties($id->id);

        $color = $params['color'];

        $sizes_data = $params['size'];

        return view('lk.products.edit', [
            'product' => $id,
            'color'=>$color,
            'sizes_data'=>$sizes_data,
            'categories'=>$categories,
            'regions'=>$regions,
            'sizes'=>$sizes,
            'colors'=>$colors,
            'statuses'=>$statuses,
            'products'=>$products,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id = null)
    {
        if($request['product-modification'] != 0){
            $product_id = $request['product-modification'];
            $is_modification = true;
        }
        else{
            $is_modification = false;
        }
        $product_data = [
            "product_id" => $product_id ?? $id->product_id,
            "name" => $request["product-name"] ?? '',
            "barcode" => $request["product-barcode"] ?? 0000000,
            "sku" => $request["product-sku"] ?? '',
            "model" => $request["product-model"],
            "country_made_in" => $request["product-region"] ?? 0,
            "description" => $request["product-description"],
            "category_id" => $request["product-category"] ?? 0,
            "category_group_id" => $request["product-base_category"] ?? 0,
            "price" => $request["product-base_price"] ?? 0,
            "dropshipping_price" => $request["product-drop_price"],
            "discount" => $request["product-discount"] ?? 0,
            "recommended_retail_price" => $request["product-price"] ?? 0,
            "min_order" => $request["product-min_order"] ?? 1,
            "status" => $request["product-status"] ?? 'hidden',
            "segment" => $request["product-segment"] ?? '',
            "stock" => array_sum($request['product-sizes'] ?? []),
            'is_modification' => $is_modification,
        ];

        ProductService::updateProduct($product_data, $id);

        return redirect()->route('lk.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function uploadFile(){
        return view('lk.products.upload');
    }
}
