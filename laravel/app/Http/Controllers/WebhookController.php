<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller {

  public function __invoke(Request $request) {

    Log::info('Payment webhook recieved:');
    Log::info($request->input());

    /** @var \App\Contracts\Gateway $gateway */
    $gateway = new $request->gateway();

    return $gateway->webhook($request);
  }

}
