<?php

namespace App\Imports;

use App\Models\Organization;
use App\Models\v1\Product;
use App\Models\v1\Region;
use App\Models\v1\Category;
use App\Services\ProductService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Illuminate\Support\Facades\Storage;
use App\Models\ProductsEnum;

class CategoriesImport implements ToModel
{
    public $parent_id;
    public $data = [];
    public $id;
    private $rows = 1;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $this->id = $this->rows++;
        for($i = 2; $i < sizeof($row); $i++){
            if(trim($row[$i]) === ""){
                return;
            }
            if(isset($this->data[$this->id])){
                foreach ($this->data as $id => $category){
                    if(trim($row[$i]) == $category['name'] && $this->data[$category['parent_id']]['name'] == trim($row[$i-1]) && $this->data[$category['parent_id']]['name'] != trim($row[$i]) && $category['parent_id'] == $this->parent_id ){
                        $this->parent_id = $id;
                        break;
                    }
                }
                $params = explode(',', trim($row[0]));
                $this->data[$this->id] = [
                    'parent_id' => $this->parent_id,
                    'name' => trim($row[$i]),
                    'slug'=>Str::slug(trim($row[$i])),
                    'type'=>trim($row[1]),
                    'params'=> json_encode($params)
                ];
            }else{
                if(empty($this->data)){
                    $params = explode(',', trim($row[0]));
                    $this->data[$this->id] = [
                        'parent_id' => 0,
                        'name' => trim($row[$i]),
                        'slug'=>Str::slug(trim($row[$i])),
                        'type'=>trim($row[1]),
                        'params'=> json_encode($params)
                    ];
                }else{
                    foreach ($this->data as $id => $category){
                        if(trim($row[$i]) == $category['name']){
                            $this->parent_id = $id;
                            break;
                        }else{
                            $this->parent_id = 0;
                        }
                    }
                    $params = explode(',', trim($row[0]));
                    $this->data[$this->id] = [
                        'parent_id' => $this->parent_id,
                        'name' => trim($row[$i]),
                        'slug'=>Str::slug(trim($row[$i])),
                        'type'=>trim($row[1]),
                        'params'=> json_encode($params)
                    ];
                }
            }
        }

    }

}
