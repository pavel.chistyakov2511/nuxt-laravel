<?php

namespace App\Imports;

use App\Models\Organization;
use App\Models\v1\Product;
use App\Models\v1\Region;
use App\Models\v1\Category;
use App\Services\ProductService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Illuminate\Support\Facades\Storage;
use App\Models\ProductsEnum;

class ProductsImport implements ToModel, WithStartRow, WithLimit
{
    private $startRow;
    private $limit;
    private $stock;

    public $lastSKU;


    public function setStartRow($startRow = 2){
        $this->startRow = $startRow;
    }
    public function setLimit($limit = 50){
        $this->limit = $limit;
    }

    public function limit(): int
    {
        return $this->limit;
    }


    public function startRow(): int
    {
        return $this->startRow;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $fields = [
            "barcode"                   => $row[0] ?? null,
            "sku"                       => trim(str_replace('\n','',$row[1])) ?? '',
            "name"                      => trim(str_replace('\n','',$row[2])) ?? '',
            "description"               => $row[3] ?? '',
            "model"                     => trim(str_replace('\n','',$row[4])) ?? '',
            "country_made_in"           => $row[5] ?? 0,
            "price"                     => $row[6] ?? 0,
            "dropshipping_price"        => $row[7] ?? 0,
            "discount"                  => $row[8] ?? 0,
            "sale"                      => $row[9] ?? 0,
            "recommended_retail_price"  => $row[10] ?? 0,
            "category_group"            => $row[11] ?? 0,
            "category"                  => $row[12] ?? 0,
            "category_type"             => $row[13] ?? 0,
            "feature"                   => $row[14] ?? '',
            "status"                    => $row[15] ?? 'Скрыто',
            "measure"                   => $row[16] ?? '',
            "collection"                => $row[17] ?? '',
            "season"                    => $row[18] ?? '',
            "segment"                   => $row[19] ?? '',
            "consist"                   => $row[20] ?? '',
            "images"                    => $row[21] ?? '',
        ];


        $product = ProductService::getProductBySKU($fields['sku']);

        if($fields['sku'] != $this->lastSKU){
            $this->stock = 0;
            $this->lastSKU = $fields['sku'];
        }else{
            $this->stock = $product->stock;
        }

        $statuses = [
            'Новинка'               =>  ProductsEnum::NEW_STATE,
            'Черновик'              =>  ProductsEnum::DRAFT_STATE,
            'Активно'               =>  ProductsEnum::ACTIVE_STATE,
            'Не активно'            =>  ProductsEnum::INACTIVE_STATE,
            'Недоступно'            =>  ProductsEnum::UNAVAILABLE_STATE,
            'Акция'                 =>  ProductsEnum::PROMO_STATE,
            'Скрыто'                =>  ProductsEnum::HIDDEN_STATE,
            'Удалено'               =>  ProductsEnum::DELETED_STATE,
            'Рекомендуемый'         =>  ProductsEnum::RECOMMENDED_STATE,
            'Ожидается в повторе'   =>  ProductsEnum::REPEAT_STATE,
            'Предзаказ'             =>  ProductsEnum::PREORDER_STATE
        ];

        $segments = [
            'Эконом'=>'low-priced',
            'Средний'=>'middle-priced',
            'Высокий'=>'high-priced',
            'Премиум'=>'luxury',
        ];

        if(!empty($fields['segment'])){
            $segment[]['name'] = $segments[$fields['segment']];
        }else{
            $segment = $fields['segment'];
        }

        if(!$product){
            $company_id = Organization::select('id')->where('user_id', Auth::id())->first();
            $category_group_id = Category::select('id','_lft','_rgt')->where('name', $fields['category_group'])->where('depth', '<=', 3)->first();
            if(!is_null($category_group_id)){
                $category_id = Category::select('id')->where('name', $fields['category'])->where('_lft', '>=',$category_group_id->_lft)->where('_rgt', '<=',$category_group_id->_rgt)->first();
                $category_group_id = $category_group_id->id;
                if(!is_null($category_id)){
                    $category_id = $category_id->id;
                }else{
                    $category_id = 0;
                }
            }else{
                $category_id = 0;
                $category_group_id = 0;
            }
            $country_id = Region::select('id')->where('name', $fields['country_made_in'])->first();
            $product_id = Str::uuid();

            $properties_data = ProductService::parseProperties($fields['feature']);

            if(!isset($properties_data['Остаток']))
                $properties_data['Остаток'] = 0;

            $product_data = [
                "product_id" => $product_id,
                "company_id" => $company_id->id,
                "name" => trim($fields['name']),
                "sku" => $fields['sku'] ?? '',
                "user_id" => Auth::id(),
                "model" => $fields['model'],
                "country_made_in" => $country_id->id ?? 0,
                "description" => $fields['description'],
                "category_id" => $category_id,
                "category_group_id" => $category_group_id,
                "price" => $fields['price'],
                "dropshipping_price" => $fields['dropshipping_price'],
                "discount" => $fields['discount'],
                "sale" =>  $fields['sale'],
                "recommended_retail_price" => (double) $fields['recommended_retail_price'],
                "status" => $statuses[$fields['status']],
                "segment" => json_encode($segment),
                "consist" => $fields['consist'],
                "measure" => $fields['measure'],
                "season"  => $fields['season'],
                "collection" => $fields['collection'],
                'stock' => $properties_data['Остаток'],
            ];


            $product = ProductService::saveProduct($product_data);

            foreach ($properties_data as $property=>$value){
                if($property == 'Цвет'){
                    $value = Str::of($value)->lower()->ucfirst();
                }
                $property_id = ProductService::findPropertyId($property, $value, $fields['category_type']);
                if($property == 'Остаток' || is_null($property_id)){
                    continue;
                }

                if($property == 'Размер'){
                    DB::insert('insert into model_property_values (property_value_id, model_type, model_id, stock, barcode, created_at) values (?, ?, ?, ?, ?, ?)', [$property_id->id, 'product', $product->id, $properties_data['Остаток'], $fields['barcode'], now()]);
                    continue;
                }

                DB::insert('insert into model_property_values (property_value_id, model_type, model_id, barcode, created_at) values (?, ?, ?, ?, ?)', [$property_id->id, 'product', $product->id, $fields['barcode'], now()]);
            }

            if(!empty($fields['images'])){
                    $img_string = $fields['images'];
                    if($img_string[-1] == ';'){
                        $img_string = substr($img_string, 0, -1);
                    }
                    $images = explode(';', $img_string);
                foreach ($images as $image){

                        $directory = 'images/products/'.$product_id;

                        $file_name = $directory."/".Str::random(25).substr($image, strripos($image, '.'));

                        $file = file_get_contents($image);
                        Storage::disk('local')->put($file_name, $file);

                        $product->addMedia(storage_path('app').'/'.$file_name)->toMediaCollection();
                }
            }

        }else{
            $category_group_id = Category::select('id','_lft','_rgt')->where('name', $fields['category_group'])->where('depth', '<=', 3)->first();
            if(!is_null($category_group_id)){
                $category_id = Category::select('id')->where('name', $fields['category'])->where('_lft', '>=',$category_group_id->_lft)->where('_rgt', '<=',$category_group_id->_rgt)->first();
                $category_group_id = $category_group_id->id;
                if(!is_null($category_id)){
                    $category_id = $category_id->id;
                }else{
                    $category_id = 0;
                }
            }else{
                $category_id = 0;
                $category_group_id = 0;
            }
            $country_id = Region::select('id')->where('name', $fields['country_made_in'])->first();

            $properties_data = ProductService::parseProperties($fields['feature']);

            if(!isset($properties_data['Остаток']))
                $properties_data['Остаток'] = 0;

            $product_data = [
                "name" => $fields['name'],
                "sku" => $fields['sku'] ?? '',
                "model" => $fields['model'],
                "country_made_in" => $country_id->id ?? 0,
                "description" => $fields['description'],
                "category_id" => $category_id,
                "category_group_id" => $category_group_id,
                "price" => $fields['price'],
                "dropshipping_price" => $fields['dropshipping_price'],
                "discount" => $fields['discount'],
                "sale" => $fields['sale'],
                "recommended_retail_price" => $fields['recommended_retail_price'],
                "status" => $statuses[$fields['status']],
                "segment" => json_encode($segment),
                "consist" => $fields['consist'],
                "measure" => $fields['measure'],
                "season"  => $fields['season'],
                "collection" => $fields['collection'],
                "stock" => $this->stock + $properties_data['Остаток'],
            ];

            ProductService::updateProduct($product_data, $product->id);

            foreach ($properties_data as $property=>$value){
                if($property == 'Цвет'){
                    $value = Str::of($value)->lower()->ucfirst();
                }
                $property_id = ProductService::findPropertyId($property, $value, $fields['category_type']);
                if($property == 'Остаток' || is_null($property_id)){
                    continue;
                }

                $findProperties = DB::table('model_property_values')
                    ->where('model_type','product')
                    ->where('property_value_id',$property_id->id)
                    ->where('model_id',$product->id)
                    ->get();

                if(count($findProperties) > 0){
                    if($property == 'Размер'){
                        DB::update(
                            'update model_property_values set stock = ?, updated_at = now() where property_value_id = ? and model_id = ?',
                            [$properties_data['Остаток'], $property_id->id, $product->id]
                        );
                        continue;
                    }
                }else{
                    if($property == 'Размер'){
                        DB::insert('insert into model_property_values (property_value_id, model_type, model_id, stock, barcode, created_at) values (?, ?, ?, ?, ?, ?)', [$property_id->id, 'product', $product->id, $properties_data['Остаток'], $fields['barcode'], now()]);
                        continue;
                    }
                    DB::insert('insert into model_property_values (property_value_id, model_type, model_id, barcode, created_at) values (?, ?, ?, ?, ?)', [$property_id->id, 'product', $product->id, $fields['barcode'], now()]);
                }
            }

            if(!empty($fields['images']) && false){
                $img_string = $fields['images'];
                if($img_string[-1] == ';'){
                    $img_string = substr($img_string, 0, -1);
                }
                $images = explode(';', $img_string);
                foreach ($images as $image){

                    $directory = 'public/'.$product->product_id;

                    $file_name = $directory."/".Str::random(25).substr($image, strripos($image, '.'));

                    $file = file_get_contents($image);
                    Storage::disk('local')->put($file_name, $file);

                    $product->addMedia(storage_path('app').'/'.$file_name)->toMediaCollection();
                }
            }
        }

    }

}
