<?php

namespace App\Imports;

use App\Models\Organization;
use App\Models\v1\Product;
use App\Models\v1\Region;
use App\Models\v1\Category;
use App\Services\ProductService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Illuminate\Support\Facades\Storage;
use App\Models\ProductsEnum;

class PropertiesImport implements ToModel
{
    public $data = [];
    private $last_type = null;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if($this->last_type != $row[0]){
            $this->last_type = $row[0];
            switch ($this->last_type){
                case 'color': $this->data['color'][] = [
                                    'category_type'=>$row[0],
                                    'title' => $row[1],
                                    'color' => $row[2]
                                ];
                                break;
                case 'texture': $this->data['texture'][] = [
                                    'category_type'=>$row[0],
                                    'title' => $row[1]
                                ];
                                break;
                default: $this->data['goods'][] = [
                                    'category_type'=>$row[0],
                                    'title' => $row[1],
                                    'age' => $row[3]
                                ];
                                break;
            }
        }else{
            switch ($this->last_type){
                case 'color': $this->data['color'][] = [
                    'category_type'=>$row[0],
                    'title' => $row[1],
                    'color' => $row[2]
                ];
                    break;
                case 'texture': $this->data['texture'][] = [
                    'category_type'=>$row[0],
                    'title' => $row[1]
                ];
                    break;
                default: $this->data['goods'][] = [
                    'category_type'=>$row[0],
                    'title' => $row[1],
                    'age' => $row[3]
                ];
                    break;
            }
        }
    }

}
