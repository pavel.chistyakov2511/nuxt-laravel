<?php

namespace App\Listeners;

use App\Events\CouponRedeemed;
use App\Models\Coupon;

class IncrementCouponTimesUsed {

  public function handle(CouponRedeemed $event) {
    Coupon::where('id', $event->coupon->id)->increment('times_used');
  }

}
