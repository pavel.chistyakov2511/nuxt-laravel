<?php

namespace App\Models;

use App\Models\v1\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Vanilo\Cart\Models\Cart as VaniloCart;
use Vanilo\Cart\Models\CartItem;
use Illuminate\Support\Facades\Auth;

class Cart extends VaniloCart
{
    use HasFactory;

    protected $fillable = [
        'stocks'
    ];

    private $cart_items;
    private $total;
    private $discount;
    private $product_discount;


    public function getItems(): Collection
    {
        $cart = Cart::select('id')->where('user_id', Auth::id())->where('state', 'active')->first();

        if(empty($cart)){
            $cart_products['price_total'] = 0;
            $cart_products['discount'] = 0;
            $cart_products['total'] = 0;
            $cart_products['status'] = 'empty';
            return collect($cart_products);
        }

        $this->cart_items = CartItem::where('cart_id', $cart->id)->get();

        $cart_products = [];
        $this->total = 0;
        $this->discount = 0;

        foreach ($this->cart_items as $cart_item){

            $product_total = 0;

            $product = Product::where('id',$cart_item['product_id'])->first();

            $product->getThumbnailUrl();
            $distributor = Distributor::where('customer_id', Auth::id())->where('seller_id', $product->company_id)->get();
            if(!empty($distributor)){
                $price = $product->price;
            }else{
                $price = $product->recommended_retail_price;
            }

            $properties = Product::properties($product->id);

            $stocks = json_decode($cart_item['stocks']);

            $color = '';

            if(isset($properties['color'])){
                $color = $properties['color'][0]->title;
            }

            if(strpos($product->discount,'%') === false){
                $discount = $product->discount;
                $product_discount = $discount;
                $is_persent = false;
            }else{
                $discount = substr($product->discount, 0, strpos($product->discount, '%'));
                $discount = round($discount / 100, 2);
                $product_discount = $price * $discount;
                $is_persent = true;
            }

            $sizes = [];

            foreach ($properties['size'] as $stock){
                $sizes[$stock->value] = 0;
            }

            foreach ($sizes as $key => $size){
                foreach ($stocks as $value=>$stock){
                    if($key == $value){
                        $sizes[$value] = $stock;
                    }
                }
            }

            $this->product_discount = 0;

            foreach ($stocks as $stock){
                $this->total += $price * $stock;

                $this->product_discount += $product_discount * $stock;

                $this->discount += $product_discount * $stock;

                $product_total += $price * $stock;
            }

            $cart_products['products'][$product->id] = [
                        'id' => $product->id,
                        'name'=> $product->name,
                        'color'=> $color,
                        'cart_sizes' => $sizes,
                        'sizes'=> $properties['size'],
                        'price'=> (float) $price,
                        'discount' => (float) $discount,
                        'product_discount'=>(float) $this->product_discount,
                        'quantity' => $cart_item['quantity'],
                        'total'=> $product_total,
                        'is_percent' => $is_persent
            ];
        }
        $cart_products['price_total'] = (float) $this->total;
        $cart_products['discount'] = (float) $this->discount;
        $cart_products['total'] = (float) $this->total - $this->discount;
        $cart_products['status'] = 'active';

        return collect($cart_products);
    }
}
