<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use HasFactory, NodeTrait;

    protected $fillable = [
    	'name',
    	'parent_id',
    	'slug'
    ];
    protected $allowedFilters = [
        'name'
    ];


    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function childrenCategories()
	{
    	return $this->hasMany(Category::class)->with('categories');
	}
}
