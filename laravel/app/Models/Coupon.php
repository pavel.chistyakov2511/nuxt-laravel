<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HandlesCoupons;

class Coupon extends Model {
  use HandlesCoupons;

  const TYPE_PERCENTAGE = 'percentage';
  const TYPE_FIXED = 'fixed';

  protected $guarded = [];

  protected $dates = [
    'valid_from',
    'valid_to',
  ];

}
