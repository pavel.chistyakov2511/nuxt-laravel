<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HandlesOrders;
use App\Traits\HandlesCartItems;

class Order extends Model {
  use HasFactory, HandlesCartItems, HandlesOrders;

  const STATUS_CART = 'cart';
  const STATUS_PENDING = 'pending';
  const STATUS_COMPLETED = 'completed';

  protected $guarded = [];

  protected $dates = [
    'paid_at'
  ];

  protected $casts = [
    'is_paid' => 'boolean',
  ];

  protected $appends = [
    'timeAgo',
  ];

  public function items() {
    return $this->hasMany(OrderItem::class);
  }

  public function coupon() {
    return $this->belongsTo(Coupon::class);
  }

  public function user() {
    return $this->belongsTo(config('user', 'App\\User'));
  }

  public function getTimeAgoAttribute() {
    return $this->created_at->diffForHumans();
  }

  public function scopeIsCart($query) {
    return $query->where('status', self::STATUS_CART);
  }

  public function scopeCompleted($query) {
    return $query->where('status', self::STATUS_COMPLETED);
  }

  protected static function boot() {
    parent::boot();

    static::deleting(function (self $cart) {
      return $cart->items()->delete();
    });
  }
}
