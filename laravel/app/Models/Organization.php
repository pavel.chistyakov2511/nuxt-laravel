<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Konekt\Address\Models\Organization as KonektOrganization;
use Spatie\MediaLibrary\InteractsWithMedia;
use Vanilo\Framework\Traits\LoadsMediaConversionsFromConfig;
use Vanilo\Support\Traits\HasImagesFromMediaLibrary;
use Spatie\MediaLibrary\HasMedia;

class Organization extends KonektOrganization implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use HasImagesFromMediaLibrary;
    use LoadsMediaConversionsFromConfig;

    protected $table = 'organizations';

    protected $fillable = ['user_id','slug','brand', 'tax_nr', 'seller_id','customer_id','registration_nr', 'email', 'phone','company_type','segment','opt', 'categories','description','min_sum_order'];
}
