<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Konekt\User\Models\UserType;

class UserEnum extends UserType
{
    const __DEFAULT = self::CLIENT;

    const ADMIN     = 'admin';
    const CLIENT    = 'client';
    const API       = 'api';
}
