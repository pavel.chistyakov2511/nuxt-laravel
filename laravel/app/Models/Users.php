<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends \Konekt\AppShell\Models\User
{
    use SoftDeletes;

    protected $enums = [
        'type' => UserEnum::class
    ];

    /**
     * Get the notes for the users.
     */
    public function notes()
    {
        return $this->hasMany('App\Notes');
    }

    protected $dates = [
        'deleted_at'
    ];
}
