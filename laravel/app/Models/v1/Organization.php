<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Konekt\Address\Models\Organization as KonektOrganization;

class Organization extends KonektOrganization
{
    use HasFactory;

    protected $table = 'organizations';

    protected $fillable = ['user_id','slug','brand', 'seller_id','customer_id','tax_nr', 'registration_nr', 'email', 'phone','company_type','segment','opt', 'categories','description','min_sum_order'];
}
