<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Region extends Model
{
    use HasFactory, NodeTrait;

    protected $fillable = [
    	'name',
    	'parent_id'
    ];
    protected $allowedFilters = [
        'name'
    ];


    public function regions()
    {
        return $this->hasMany(Region::class);
    }

    public function childrenRegions()
	{
    	return $this->hasMany(Region::class)->with('regions');
	}

    public static function scopeCountries()
    {
        return Region::where('depth', 1)->get();
    }
}
