<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'company_name',
        'email' ,
        'phone',
        'firstname',
        'lastname',
        'registration_nr',
        'tax_nr',
        'is_active'
    ];
}
