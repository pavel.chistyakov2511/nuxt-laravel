<?php

namespace App\Services;

use App\Models\v1\Category;
use App\Models\ProductModel;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;

class CategoryService
{
    private static $depthFrom = 1;
    private static $depthTo = 3;
    private static $status = 0;
    public static $categories = [];

    public function __construct()
    {

    }

    public static function getCategoryByName($categoryName){
        $category = Category::where('name', $categoryName)->first();
        if(empty($category)){
            return null;
        }
        return $category;
    }

    public static function getBaseCategores(){
        $baseCategories = Cache::rememberForever('baseCategories', function () {
            return Category::where('depth', '>=', self::$depthFrom)->where('depth', '<=', self::$depthTo)->where('status', self::$status)->hasChildren()->get();
        });
        return $baseCategories;
    }

    public static function getLastCategories(){

        $categories = Cache::rememberForever('lastCategories', function () {
            $baseCategories = self::getBaseCategores();
            foreach($baseCategories as $parent_category){
                self::$categories[$parent_category->id] = Category::where('_lft', '>', $parent_category->_lft)
                                                                  ->where('_rgt', '<', $parent_category->_rgt)
                                                                  //->whereIsLeaf()
                                                                  ->where('status', self::$status)
                                                                  ->get();
            }
            return self::$categories;
        });

        return $categories;
    }
}
