<?php

namespace App\Services;

use App\Models\v1\Product;
use Vanilo\Order\Contracts\Order;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Vanilo\Checkout\Contracts\Checkout;
use Vanilo\Contracts\CheckoutSubject;
use Vanilo\Framework\Factories\OrderFactory;
use Vanilo\Order\Contracts\OrderNumberGenerator;
use Vanilo\Order\Events\BaseOrderEvent;
use Vanilo\Order\Events\OrderWasCreated;
use Vanilo\Order\Exceptions\CreateOrderException;
use App\Models\Cart;
use Vanilo\Properties\Models\PropertyValue;

class OrderService extends OrderFactory
{


    public function __construct(OrderNumberGenerator $generator)
    {
        $this->orderNumberGenerator = $generator;
    }

    public function createFromCheckout(Checkout $checkout)
    {
        $orderData = [
            'billpayer'       => $checkout->getBillpayer()->toArray(),
            'shippingAddress' => $checkout->getShippingAddress()->toArray()
        ];

        $items = $this->convertCartItemsToDataArray($checkout->getCart());

        return $this->createFromDataArray($orderData, $items);
    }

    protected function convertCartItemsToDataArray(CheckoutSubject $cart)
    {
        return $cart->getItems()->map(function ($item) {
            return [
                'product'  => $item->getBuyable(),
                'quantity' => $item->getQuantity()
            ];
        })->all();
    }


    public function createFromDataArray(array $data, array $items): \Vanilo\Order\Contracts\Order
    {
        if (empty($items)) {
            throw new CreateOrderException(__('Can not create an order without items'));
        }

        DB::beginTransaction();

        try {
            $order = app(Order::class);

            //$order->fill(Arr::except($data, ['billpayer', 'shippingAddress']));
            $order->number  = $data['number'] ?? $this->orderNumberGenerator->generateNumber($order);
            $order->user_id = $data['user_id'] ?? auth()->id();

            $order->save();

            $this->createBillpayer($order, $data);
            $this->createShippingAddress($order, $data);

            foreach ($items as $item) {

                    $product = Product::find($item['product_id']);

                    $order->items()->create([
                        'order_id' => $order->id,
                        'product_type' => 'product',
                        'model_type' => 'product',
                        'model_id' => $product->id,
                        'company_id' => $product->company_id,
                        'product_id' => $product->id,
                        'price' => $item['price'],
                        'name' => $product->name,
                        'options' => $item['stocks'],
                        'title' => "title",
                        'quantity' => 1,
                        'status' => 'pending',
                    ]);
            }

            $order->save();
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        event(new OrderWasCreated($order));

        return $order;
    }

    public static function getQuantity($order_item){
        $quantity = 0;
        foreach ($order_item as $item){
            $quantity += $item;
        }
        return $quantity;
    }

    public static function getOrderSizeList($order_item){
        $sizes = [];

        foreach ($order_item as $value=>$stock){
            $size = PropertyValue::select('title')->where('value', $value)->first();
            $sizes[] = ['value'=>$size->title, 'stock'=>$stock];
        }

        return $sizes;
    }

}
