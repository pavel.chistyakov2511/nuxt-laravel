<?php

namespace App\Services;

use App\Models\v1\Product;
use App\Models\v1\Tender;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductImage;
use App\Models\v1\Category;
use Illuminate\Support\Facades\DB;

class ProductService
{

    public static function getCoverImages($products){

        $covers = [];

        for($i = 0; $i < count($products); $i++){
            $covers[$products[$i]['product_id']] = ProductImage::select('product_id', 'img_url')->where('product_id', $products[$i]['product_id'])->where('is_cover', true)->first();
            if($covers[$products[$i]['product_id']] == null)
                unset($covers[$products[$i]['product_id']]);
        }
        return $covers;
    }

    public static function getProducts(){

            $products = Product::where('user_id', Auth::id())
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(30);

        $covers = self::getCoverImages($products);
        foreach ($products as $key=>$value){
            foreach ($covers as $k=>$v){
                if($value['product_id'] == $v['product_id']){
                    $products[$key]['cover'] = $covers[$k]['img_url'];
                }
            }
        }

        return $products;
    }

    public static function saveProduct($param)
    {
        $product = Product::create($param);

        return $product;
    }

    public static function updateProduct($param, $product_id)
    {
        $product = Product::where('id',$product_id)->update($param);

        return $product;
    }

    public static function getProperties($product_id){
        $properties = DB::table('model_property_values')
                        ->join('property_values', 'model_property_values.property_value_id', '=', 'property_values.id')
                        ->join('properties', 'property_values.property_id', '=', 'properties.id')
                        ->select('model_property_values.stock','model_property_values.barcode', 'properties.slug', 'property_values.id', 'property_values.value','property_values.title')
                        ->where('model_type','product')
                        ->where('model_id', $product_id)
                        ->get();

        $data = [];

        foreach((array)$properties as $property){
            foreach($property as $key => $value){
                $data[$value->slug][] = $property[$key];
            }
        }

        return $data;
    }

    public static function getProductSizes($product){
        $properties = DB::table('property_values')
            ->select('property_values.id', 'property_values.value','property_values.title')
            ->where('category_type',$product->category_type)
            ->where('model_id', $product->id)
            ->get();

        $data = [];

        foreach((array)$properties as $property){
            foreach($property as $key => $value){
                $data[$value->slug][] = $property[$key];
            }
        }

        return $data;
    }

    public static function getTenderProductProperty($sizes = []){

        $products_of_property = [];

        foreach ($sizes as $size){
            $products_size_zero = DB::table('model_property_values')
                ->where('model_type','product')
                ->where('property_value_id',$size)
                ->where('stock','>',0)
                ->get();

            if(empty($products_size_zero))
                continue;

            foreach ($products_size_zero as $data){
                if(isset($products_of_property[$data->model_id])){
                    $products_of_property[$data->model_id] += 1;
                }else{
                    $products_of_property[$data->model_id] = 1;
                }
            }

        }

        unset($data);

        $data = [];

        foreach ($products_of_property as $k=>$v){
            $data[] = $k;
        }

        return $data;
    }

    public static function getProductBySKU($sku){
        return Product::select('id','product_id','stock')->where('sku', $sku)->where('user_id', Auth::id())->first();
    }

    public static function parseProperties($properties){

        $property_data = [];

        if(!empty($properties)){
            if($properties[-1] == ';'){
                $properties = substr($properties, 0, -1);
            }
            $prop_arr = explode(';', $properties);
            foreach ($prop_arr as $property){
                $prop = explode('|', $property);
                $property_data[$prop[0]] = $prop[1];
            }

        }
        return $property_data;
    }

    public static function findPropertyId($property_name, $property_value, $category_type){
        if($property_name == 'Размер'){
            $properties = DB::table('property_values')
                ->join('properties', 'property_values.property_id', '=', 'properties.id')
                ->select('property_values.id')
                ->where('properties.name',$property_name)
                ->where('property_values.title', $property_value)
                ->where('property_values.category_type', $category_type)
                ->first();
        }else{
            $properties = DB::table('property_values')
                ->join('properties', 'property_values.property_id', '=', 'properties.id')
                ->select('property_values.id')
                ->where('properties.name',$property_name)
                ->where('property_values.title', $property_value)
                ->first();
        }

        return $properties;
    }
}
