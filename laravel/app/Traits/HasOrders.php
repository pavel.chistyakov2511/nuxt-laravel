<?php

namespace App\Traits;

use App\Models\Order;
use App\Models\OrderItem;

trait HasOrders {

  public function orders() {
    return $this->hasMany(Order::class)->where('status', Order::STATUS_COMPLETED);
  }

  public function orderItems() {
    return $this->hasManyThrough(OrderItem::class, Order::class)->where('orders.status', Order::STATUS_COMPLETED);
  }

}
