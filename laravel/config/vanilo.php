<?php

return [
    'framework' => [
        'image'       => [
            'product' => [
                'variants' => [
                    'default' => [ // Name of the image variant
                        'width'  => 800,
                        'height' => 800,
                        'fit'    => 'crop'
                    ],
                    'thumbnail' => [ // Name of the image variant
                        'width'  => 800,
                        'height' => 800,
                        'fit'    => 'crop'
                    ],
                    'cart' => [ // Image variant names can be arbitrary
                        'width'  => 120,
                        'height' => 90,
                        'fit'    => 'crop'
                    ]
                ]
            ],
            'taxon' => [
                'variants' => [
                    'thumbnail' => [
                        'width'  => 320,
                        'height' => 180,
                        'fit'    => 'crop'
                    ],
                    'banner' => [
                        'width'  => 1248,
                        'height' => 702,
                        'fit'    => 'crop'
                    ]
                ]
            ]
        ]
    ],
    'user' => [
        'model' => App\Models\v1\User::class,
    ],
    'order' =>[
        'number'=>[
            'generator' => 'sequential_number',
            'sequential_number' => [
                'start_sequence_from' => 1,
                'prefix'              => 'PO-',
                'pad_length'          => 10,
                'pad_string'          => '0'
            ]
        ]
    ]
];
