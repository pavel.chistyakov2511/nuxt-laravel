<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->bigInteger('seller_id')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->string('slug');
            $table->string('brand');
            $table->string('tax_nr', 17)->nullable()->comment('Tax/VAT Identification Number'); //https://www.wikiwand.com/en/VAT_identification_number
            $table->string('registration_nr')->nullable()->comment('Company/Trade Registration Number');
            $table->string('email')->nullable();
            $table->string('phone', 22)->nullable();
            $table->enum('company_type',['manufacturer','customer']);
            $table->double('min_sum_order');
            $table->jsonb('segment');
            $table->jsonb('opt');
            $table->jsonb('categories');
            $table->boolean('dropshipping_enabled')->nullable()->default(false);
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organizations');
    }
}
