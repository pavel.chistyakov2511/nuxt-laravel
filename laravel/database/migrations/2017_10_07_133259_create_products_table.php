<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ProductsEnum;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('product_id');
            $table->bigInteger('price_product_id')->nullable();
            $table->bigInteger('company_id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('sku');
            $table->text('excerpt')->nullable();
            $table->text('description')->nullable();
            $table->enum('status', ProductsEnum::values())->default(ProductsEnum::defaultValue());
            $table->string('ext_title', 511)->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->integer('user_id');
            $table->string('model')->nullable();
            $table->string('measure')->nullable();
            $table->jsonb('segment');
            $table->string('season')->nullable();
            $table->string('collection')->nullable();
            $table->string('consist')->nullable();
            $table->integer('min_order')->nullable()->default(1);
            $table->integer('country_made_in')->nullable()->default(0);
            $table->integer('modification')->nullable();
            $table->integer('category_group_id')->default(0);
            $table->integer('category_id')->default(0);
            $table->integer('stock')->default(0);
            $table->integer('count_show')->default(0); // Количество раз товар был показан покупателям
            $table->integer('count_order')->default(0); // Количество раз товар был закан покупателям
            $table->decimal('price', 15, 2)->nullable();
            $table->decimal('dropshipping_price', 15, 2)->nullable();
            $table->string('discount')->nullable();
            $table->integer('sale')->nullable();
            $table->decimal('recommended_retail_price', 15, 2);
            $table->boolean('is_was_email_send')->default(false);
            $table->boolean('is_modification')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
