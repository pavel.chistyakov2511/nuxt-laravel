<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Order;

class CreateOrdersTable extends Migration {

  public function up() {
    Schema::create('orders', function (Blueprint $table) {

      $table->id();
      $table->foreignId('coupon_id')->nullable();

      $table->string('status')->default(Order::STATUS_CART);
      $table->string('currency')->default(config('commerce.currency'));
      $table->timestamp('paid_at')->nullable();

      $table->integer('items_total')->default(0);
      $table->integer('tax_total')->default(0);
      $table->integer('shipping_total')->default(0);
      $table->integer('coupon_total')->default(0);
      $table->integer('grand_total')->default(0);

      $table->string('gateway')->nullable();
      $table->json('gateway_data')->nullable();

      $table->string('number', 32);
            $table->intOrBigIntBasedOnRelated('user_id', Schema::connection(null), 'users.id')->nullable();
            $table->integer('billpayer_id')->unsigned()->nullable();
            $table->integer('shipping_address_id')->unsigned()->nullable();
            $table->text('notes')->nullable();

      $table->timestamps();
      $table->softDeletes();

      $table->foreign('user_id')
                ->references('id')
                ->on('users');

      $table->foreign('billpayer_id')
                  ->references('id')
                  ->on('billpayers');

      $table->foreign('shipping_address_id')
                  ->references('id')
                  ->on('addresses');
    });
  }

  public function down() {
    Schema::dropIfExists('orders');
  }
}
