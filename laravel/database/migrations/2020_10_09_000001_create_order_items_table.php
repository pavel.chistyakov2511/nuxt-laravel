<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Order;

class CreateOrderItemsTable extends Migration {

  public function up() {
    Schema::create('order_items', function (Blueprint $table) {

      $table->id();
      $table->integer('order_id')->unsigned();
      $table->string('product_type');
      $table->integer('product_id')->unsigned();
      $table->integer('company_id')->unsigned();
      $table->string('name')->nullable()->comment('The product name at the moment of buying');
      $table->morphs('model');
      $table->unsignedInteger('quantity')->default(1);
      $table->json('options')->nullable();
      $table->string('title');
      $table->decimal('price', 15, 4);
      $table->integer('discount')->default(0);
      $table->string('status')->default(Order::STATUS_CART);
      $table->timestamps();

      $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');

    });
  }

  public function down() {
    Schema::dropIfExists('order_items');
  }

}
