<?php

namespace Database\Seeders;

use App\Imports\CategoriesImport;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Models\v1\Category;
use Illuminate\Support\Str;
use App\Http\Controllers\Api\v1\CategoriesController;
use Maatwebsite\Excel\Facades\Excel;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $import = new CategoriesImport();
        $import->id = 1;
        $import->parent_id = 0;

        $path = storage_path('app').'/framework/categoriesUpload/categories.xlsx';

        Excel::import($import, $path);

        $CategoryRepository = new Category;

        foreach ($import->data as $categoryData){
            $inserted = $CategoryRepository->create($categoryData);
            $result = Category::withDepth()->find($inserted->id);
            $category = Category::find($inserted->id);
            $category->depth = $result->depth;
            $category->save();
        }
    }
}
