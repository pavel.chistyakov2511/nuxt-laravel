<?php

namespace Database\Seeders;

use App\Imports\ProductsImport;
use App\Imports\PropertiesImport;
use App\Models\v1\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use Vanilo\Properties\Models\Property;

class PropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $import = new PropertiesImport();

        $path = storage_path('app').'/framework/propertiesUpload/properties.xlsx';

        Excel::import($import, $path);

        foreach ($import->data as $key=>$propertyData){
            if($key == 'color'){
                $color = Property::create(['name' => 'Цвет', 'slug'=>'color', 'type' => 'text']);
                foreach ($propertyData as $colorData){
                    $color->propertyValues()->create($colorData);
                }
            }
            else if($key == 'texture'){
                $texture = Property::create(['name' => 'Материал', 'slug'=>'texture', 'type' => 'text']);
                foreach ($propertyData as $textureData){
                    $texture->propertyValues()->create($textureData);
                }
            }else{
                $sizes = Property::create(['name' => 'Размер', 'slug'=>'size', 'type' => 'text','configuration'=>'{"type":"Шт."}']);
                foreach ($propertyData as $sizesData){
                    $sizes->propertyValues()->create($sizesData);
                }
            }
        }
    }
}
