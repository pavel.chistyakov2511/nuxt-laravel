<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PassportAuthController;
//use App\Http\Controllers\PostController;
//use App\Http\Controllers\CategoriesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*Route::middleware('guest')->group(function () {
    Route::post('register', 'AuthController@register')->name('register');
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('refresh-token', 'AuthController@refreshToken')->name('refreshToken');
});*/

Route::prefix('v1')->group(function(){
    Route::post('register', [PassportAuthController::class, 'register']);
    Route::post('login', [PassportAuthController::class, 'login']);
});

Route::middleware('auth:api')->group(function () {
    Route::prefix('v1')->group(function(){
        Route::prefix('products')->group(function(){
            Route::post('upload', [\App\Http\Controllers\Api\v1\ProductsController::class, 'uploadFile']);
            Route::post('show', [\App\Http\Controllers\Api\v1\ProductsController::class, 'show']);
            Route::post('edit', [\App\Http\Controllers\Api\v1\ProductsController::class, 'edit']);
            Route::post('store', [\App\Http\Controllers\Api\v1\ProductsController::class, 'store']);
            Route::post('update', [\App\Http\Controllers\Api\v1\ProductsController::class, 'update']);
            Route::get('create', [\App\Http\Controllers\Api\v1\ProductsController::class, 'create']);
            Route::get('list', [\App\Http\Controllers\Api\v1\ProductsController::class, 'sizesList']);
            Route::post('remove_img', [\App\Http\Controllers\Api\v1\ProductsController::class, 'removeImg']);
            Route::get('/', [\App\Http\Controllers\Api\v1\ProductsController::class, 'index']);
        });
        Route::prefix('carts')->group(function(){
            Route::post('save', [App\Http\Controllers\CartController::class, 'store']);
            Route::get('/', [App\Http\Controllers\CartController::class, 'index']);
        });
        Route::get('/category/list', [\App\Http\Controllers\api\v1\CategoriesController::class, 'list']);
        Route::get('/regions/list', [\App\Http\Controllers\api\v1\RegionsController::class, 'list']);
        //Route::resource('categories', CategoriesController::class);
        Route::resource('user', UserController::class);
        Route::post('logout', [PassportAuthController::class, 'logout'])->name('logout');

        Route::prefix('menu')->group(function(){
            Route::get('get', [\App\Http\Controllers\API\v1\MenuController::class, 'index']);
        });
    });
});
